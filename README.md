# schedules-manager

## Descrição

Projeto inicialmente desenvolvido para cumprir demanda da primeira atividade proposta pelos especialistas. Foi diminuído o escopo para facilitar o desenvolvimento para disciplina de Devops, como por exemplo: não há operações de modifição no banco e o banco original, postgresql, foi subistituido por um banco em memória (H2).

A aplicação se propõe a simular um gerenciador de pautas. Neste recorte só está disponível as operações de consulta.

## Pipeline
### Quality (lint)
Utiliza o Sonar Cloud

disponível no [link](https://sonarcloud.io/organizations/devops-tst/projects)
### Build
Build com imagem Docker [maven:3.8.6-amazoncorretto-17](https://hub.docker.com/_/maven)
### Test
Testes com a mesma imagem acima
### Package
Empacotamento da imagem utilizando o binário .jar construído na etapa **Build**

Utiliza as images Docker [docker:20.10.21 + 20.10.21-dind](https://hub.docker.com/_/docker) como builder e e serviço, respectivamente.
### Deploy
Realiza a disponibilização do aplicativo na plataforma Google Cloud utulizando de Clusters Kubernetes.

Utiliza a imagem [bitnami/kubectl](https://hub.docker.com/r/bitnami/kubectl) que executa a criação de 3 pods e 1 serviço do tipo LoadBalancer.

## Dependências para desenvolvimento local
### Spring Boot version
`2.7.3`

### System dependencies

#### Java
Java 17 ou superior
*para instalação no Ubuntu:*
```sh
sudo apt update

sudo apt install openjdk-17-jdk
```

#### Postgresql (Originalmente)
*para instalação no Ubuntu:*
```sh
sudo apt -y install postgresql postgresql-contrib

sudo passwd postgres

sudo service postgresql start

sudo -sudo -u postgres psql
# postgres=# CREATE USER your_username WITH ENCRYPTED PASSWORD 'password';
# postgres=# CREATE ROLE your_username;
# postgres=# ALTER ROLE your_username WITH LOGIN SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;
# postgres=# SET ROLE your_username;
# postgres=# GRANT postgres TO your_username;
# postgres=# CREATE DATABASE schedules_manager ENCODING 'UTF8' TEMPLATE template0 OWNER your_username;
# postgres=# \q

# create env variables
export DB_USERNAME="your_username"
export DB_PASSWORD="password"
``` 

#### Maven
*para instalação no Ubuntu:*
```sh
sudo apt install maven
```
